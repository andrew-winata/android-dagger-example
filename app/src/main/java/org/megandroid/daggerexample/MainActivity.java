package org.megandroid.daggerexample;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.megandroid.daggerexample.services.GitHubService;
import org.megandroid.daggerexample.services.model.GitHubUserDetailResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
  GitHubService gitHubService;
  Toolbar toolbar;
  View contentView;
  TextView txtContent;
  static final String TAG="MainActivity";
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    contentView = findViewById(R.id.main_content);
    txtContent =(TextView) findViewById(R.id.txt_content);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
      }
    });

    Call<GitHubUserDetailResponse> call =  MyApplication.getInstance().getGitHubService().getUserByUsername("andrew-winata");
    call.enqueue(new Callback<GitHubUserDetailResponse>() {
      @Override
      public void onResponse(Call<GitHubUserDetailResponse> call, Response<GitHubUserDetailResponse> response) {
        GitHubUserDetailResponse responseModel = response.body();
        txtContent.setText(responseModel.getName());
      }

      @Override
      public void onFailure(Call<GitHubUserDetailResponse> call, Throwable t) {
        Log.e(TAG,"Error get github user", t);
        Snackbar.make(contentView,"Error get github user", Snackbar.LENGTH_LONG);
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
