package org.megandroid.daggerexample;

import android.app.Application;
import android.content.SharedPreferences;

import org.megandroid.daggerexample.di.dagger.components.ApplicationComponent;
import org.megandroid.daggerexample.di.dagger.components.DaggerApplicationComponent;
import org.megandroid.daggerexample.services.GitHubService;

/**
 * Created by andrew.winata on 8/22/2017.
 */

public class MyApplication extends Application {
  SharedPreferences sharedPreferences;
  GitHubService gitHubService;
  ApplicationComponent applicationComponent;
  private static MyApplication mInstance;

  public static MyApplication getInstance() {
    return mInstance;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    mInstance = this;
    applicationComponent = DaggerApplicationComponent.builder().build();
    applicationComponent.inject(this);
    gitHubService = applicationComponent.gitHubService();
  }

  public SharedPreferences getSharedPreferences() {
    return sharedPreferences;
  }

  public GitHubService getGitHubService() {
    return gitHubService;
  }
}
