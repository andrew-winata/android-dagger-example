package org.megandroid.daggerexample.di.dagger.components;

import android.app.Application;

import org.megandroid.daggerexample.MainActivity;
import org.megandroid.daggerexample.di.dagger.annotations.ApplicationScope;
import org.megandroid.daggerexample.di.dagger.modules.GitHubServiceModule;
import org.megandroid.daggerexample.di.dagger.modules.WebServiceModule;
import org.megandroid.daggerexample.services.GitHubService;

import dagger.Component;

/**
 * Created by andrew.winata on 8/23/2017.
 */
@Component(modules = {WebServiceModule.class, GitHubServiceModule.class})
@ApplicationScope
public interface ApplicationComponent {
  void inject(MainActivity activity);
  void inject(Application application);
  GitHubService gitHubService();
}
