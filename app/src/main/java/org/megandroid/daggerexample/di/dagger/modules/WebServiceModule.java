package org.megandroid.daggerexample.di.dagger.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.megandroid.daggerexample.di.dagger.annotations.ApplicationScope;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andrew.winata on 8/23/2017.
 */
@Module
public class WebServiceModule {

  @Provides
  @ApplicationScope
  HttpLoggingInterceptor providesHttpLogginInterceptor() {
    HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
    httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return httpLoggingInterceptor;
  }

  @Provides
  @ApplicationScope
  Gson providesGson() {
    return new GsonBuilder().create();
  }

  @Provides
  @ApplicationScope
  OkHttpClient providesOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
    return new OkHttpClient.Builder().connectTimeout(1L, TimeUnit.MINUTES)
        .writeTimeout(1L, TimeUnit.MINUTES).readTimeout(1L, TimeUnit.MINUTES)
        .addInterceptor(httpLoggingInterceptor).build();
  }

  @Provides
  @ApplicationScope
  Retrofit providesRetrofit(OkHttpClient httpClient, Gson gson) {
    return new Retrofit.Builder().baseUrl("https://api.github.com")
            .addConverterFactory(GsonConverterFactory.create(gson)).client(httpClient) // custom client
            .build();
  }
}
