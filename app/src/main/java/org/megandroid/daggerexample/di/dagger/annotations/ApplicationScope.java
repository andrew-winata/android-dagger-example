package org.megandroid.daggerexample.di.dagger.annotations;

import javax.inject.Scope;

/**
 * Created by andrew.winata on 8/23/2017.
 */
@Scope
public @interface ApplicationScope {
}
