package org.megandroid.daggerexample.di.dagger.modules;

import org.megandroid.daggerexample.di.dagger.annotations.ApplicationScope;
import org.megandroid.daggerexample.services.GitHubService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by andrew.winata on 8/23/2017.
 */
@Module(includes = {WebServiceModule.class})
public class GitHubServiceModule {

  @Provides
  @ApplicationScope
  GitHubService providesGitHubService(Retrofit retrofit) {
    return retrofit.create(GitHubService.class);
  }
}
