package org.megandroid.daggerexample.services;

import org.megandroid.daggerexample.services.model.GitHubUserDetailResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

/**
 * Created by andrew.winata on 8/22/2017.
 */

public interface GitHubService {
  @Headers({"Accept: application/json", "Content-Type: application/json"})
  @GET("/users/{username}")
  Call<GitHubUserDetailResponse> getUserByUsername(@Path("username") String username);
}
